#!/bin/bash
# conda activate Hintermannetal2022

getFromGEO () {
    # $1 is the file to get
    # $2 is the folder/ where to save, if specified.
    # For example getFromGEO GSM5835468_ChIP_CTCF_E105_PT_rep1.bigwig downloads/ChIP/
    GEOnb=$(echo "$1" | cut -d '_' -f 1)
    if [ "${GEOnb:2:1}" = E ]; then
        GStype="series"
    elif [ "${GEOnb:2:1}" = M ]; then
        GStype="samples"
    else
        echo "GStype not properly defined"
    fi
    echo "wget ftp://ftp.ncbi.nlm.nih.gov/geo/$GStype/${GEOnb:0:$((${#GEOnb}-3))}nnn/$GEOnb/suppl/$1 -O $2${1/${GEOnb}_/} -nc"
    wget ftp://ftp.ncbi.nlm.nih.gov/geo/$GStype/${GEOnb:0:$((${#GEOnb}-3))}nnn/$GEOnb/suppl/$1 -O $2${1/${GEOnb}_/} -nc
}

# To be able to use it in other bash scripts
export -f getFromGEO

# Put here the path to the git directory:
workingDir="$PWD/"
cd $workingDir

mkdir -p intermediateFiles
mkdir -p downloads
mkdir -p savedOutputs
mkdir -p savedOutputs/plots
mkdir -p savedOutputs/files

######
### Gene annotations for all Figures
######

# Choose Ensembl or Custom as argument
# Ensembl: gtf is downloaded from ensembl and processed to generate the custom gtf
# Custom: custom gtf is downloaded from Zenodo

#./scripts/GTF.sh ensembl
./scripts/GTF.sh custom

# CHiC - ice correction on raw cool files and TAD calling
./scripts/CHiC_processRaws.sh

######
### Figure 2
######

# Get CTCF ChIP-seq data
mkdir -p downloads/ChIP

getFromGEO GSM5835468_ChIP_CTCF_E105_PT_rep1.bigwig downloads/ChIP/
getFromGEO GSM5835469_ChIP_CTCF_HH18_PT_rep1.bigwig downloads/ChIP/

# Mouse CHiC heatmap
pyGenomeTracks --tracks plots/fig2_mCHiC.ini -o savedOutputs/plots/fig2_mCHiC.pdf --region chr2:73800000-75800000 --width 18.5 --dpi 600
# Chicken CHiC heatmap
pyGenomeTracks --tracks plots/fig2_cCHiC.ini -o savedOutputs/plots/fig2_cCHiC.pdf --region chr7:15790000-16700000  --dpi 600 --width 18.5 --decreasingXAxis

######
### Figure 3
######

# Get ChIP coverage data
geoNames_forScans="GSM5835473_ChIP_H3K27ac_E125_WP_rep1 GSE194418_ChIP_H3K27ac_E85_PT_rep1rep2 GSM5835471_ChIP_H3K27ac_E125_FB_rep1 GSE194418_ChIP_H3K27ac_HH35_DS_rep1rep2 GSE194418_ChIP_H3K27ac_HH18_PT_rep1rep2 GSM5835483_ChIP_H3K27ac_HH35_FB_rep1"

for n in $geoNames_forScans; do 
   getFromGEO "$n".bigwig downloads/ChIP/
done

# Find the region of TDOM which is the densest acetylated region 

python scripts/ChIP_scanRegionForHighestDensity_2files.py "downloads/ChIP/ChIP_H3K27ac_E125_WP_rep1.bigwig" "downloads/ChIP/ChIP_H3K27ac_E125_FB_rep1.bigwig" "chr2" 74775737 75599470 .3 2000 10000
python scripts/ChIP_scanRegionForHighestDensity_2files.py "downloads/ChIP/ChIP_H3K27ac_E85_PT_rep1rep2.bigwig" "downloads/ChIP/ChIP_H3K27ac_E125_FB_rep1.bigwig" "chr2" 74775737 75599470 .5 2000 10000

python scripts/ChIP_scanRegionForHighestDensity_2files.py "downloads/ChIP/ChIP_H3K27ac_HH35_DS_rep1rep2.bigwig" "downloads/ChIP/ChIP_H3K27ac_HH35_FB_rep1.bigwig" "chr7" 15856252 16252401 .3 2000 10000
python scripts/ChIP_scanRegionForHighestDensity_2files.py "downloads/ChIP/ChIP_H3K27ac_HH18_PT_rep1rep2.bigwig" "downloads/ChIP/ChIP_H3K27ac_HH35_FB_rep1.bigwig" "chr7" 15856252 16252401 .5 2000 10000

mv *MAR.bed savedOutputs/files/
mv regionForWindows_chr[27].bed savedOutputs/files/

# Mouse and chicken H3K27ac coverage at HoxD
pyGenomeTracks --tracks plots/fig3_mChIP.ini -o savedOutputs/plots/fig3_mChIP.pdf --region chr2:73800000-75800000 --width 18.5 --dpi 600 
pyGenomeTracks --tracks plots/fig3_cChIP.ini -o savedOutputs/plots/fig3_cChIP.pdf --region chr7:15790000-16700000 --dpi 600 --width 18.5 --decreasingXAxis

######
### Figure 4F
######

# Get and unzip ChIP macs2 peaks
geoNames_forPeaks="$geoNames_forScans GSM5835470_ChIP_H3K27ac_E125_DFL_rep1 GSM5835472_ChIP_H3K27ac_E125_PFL_rep1 GSM5835480_ChIP_H3K27ac_HH28_PFL_rep1 GSM5835479_ChIP_H3K27ac_HH28_DFL_rep1"

for n in $geoNames_forPeaks; do 
   getFromGEO "$n".narrowPeak.gz downloads/ChIP/
done

gunzip -k downloads/ChIP/*.gz

mkdir downloads/ATAC/
# Get and unzip ATAC macs2 peaks and coverage
geoNames_forATAC="GSM6152750_ATAC_E95_PT_rep1 GSM6152751_ATAC_E95_PT_rep2 GSM6152752_ATAC_E125_FB_rep1 GSM6152753_ATAC_E125_FB_rep2 GSM6152754_ATAC_E125_WP_rep1 GSM6152755_ATAC_E125_WP_rep2 GSM6152756_ATAC_HH19_PT_rep1 GSM6152757_ATAC_HH19_PT_rep2 GSM6152758_ATAC_HH35_DS_rep1 GSM6152759_ATAC_HH35_DS_rep2 GSM6152760_ATAC_HH35_FB_rep1 GSM6152761_ATAC_HH35_FB_rep2"

for n in $geoNames_forATAC; do 
   getFromGEO "$n".narrowPeak.gz downloads/ATAC/
   getFromGEO "$n".bigwig downloads/ATAC/
done

gunzip -k downloads/ATAC/*.gz

# Normalize and average ATAC coverage of replicates
python scripts/ATAC_normBigWig.py $workingDir

# Mouse H3K27ac and ATACseq coverage in the D1 region
pyGenomeTracks --tracks plots/fig4_mChIP.ini -o savedOutputs/plots/fig4_mChIP.pdf --region chr2:74747751-74916570 --width 10.5 --dpi 600

######
### CNEs (Figure 5, Figure S4)
######

./scripts/CNEs_01.sh $workingDir

### R plots for Figure 5 are saved in savedOutputs/plots.

### Figure S4 - Mouse and chicken H3K27ac coverage at in D1-D9 regions
pyGenomeTracks --tracks plots/figS4_mChIP.ini -o savedOutputs/plots/figS4_mChIP.pdf --region chr2:74775737-75222876 --width 18.5 --dpi 600
pyGenomeTracks --tracks plots/figS4_cChIP.ini -o savedOutputs/plots/figS4_cChIP.pdf --region chr7:16033612-16252401 --dpi 600 --width 18.5 --decreasingXAxis

######
### 4C (Figure S3)
######
# CTCF_orientation.sh (Figure S3) - find orientation and motif scores of CTCF peaks
./scripts/ChIP_CTCF_orientation.sh 

# The first steps of 4C analysis is in the 4C_01_fromFastqToCoverage directory.

mkdir -p downloads/4C

geoNames_4C="GSM5841325_4C_Hoxd1_E95_PT_rep1 GSE195591_4C_Hoxd4_E95_PT_neq2 GSE195591_4C_Hoxd9_E95_PT_neq2 GSE195591_4C_Hoxd1_E125_FB_neq2 GSE195591_4C_Hoxd4_E125_FB_neq2 GSE195591_4C_Hoxd9_E125_FB_neq2 GSM5841336_4C_HOXD1_HH18_PT_rep1 GSM5841337_4C_HOXD4_HH18_PT_rep1 GSM5841338_4C_HOXD9_HH18_PT_rep1 GSM5841339_4C_HOXD1_HH35_FB_rep1 GSM5841340_4C_HOXD4_HH35_FB_rep1 GSM5841341_4C_HOXD9_HH35_FB_rep1"
for n in $geoNames_4C; do
    getFromGEO "$n".bedgraph.gz downloads/4C/
done

geoNames_SegToFrags="GSM5841325_segToFrag_4C_Hoxd1_E95_PT_rep1 GSM5841326_segToFrag_4C_Hoxd4_E95_PT_rep1 GSM5841331_segToFrag_4C_Hoxd4_E95_PT_rep2 GSM5841327_segToFrag_4C_Hoxd9_E95_PT_rep1 GSM5841332_segToFrag_4C_Hoxd9_E95_PT_rep2 GSM5841336_segToFrag_4C_HOXD1_HH18_PT_rep1 GSM5841337_segToFrag_4C_HOXD4_HH18_PT_rep1 GSM5841338_segToFrag_4C_HOXD9_HH18_PT_rep1"
for n in $geoNames_SegToFrags; do
    getFromGEO "$n".bw downloads/4C/
done

gunzip -k downloads/4C/*.gz

# Subset smoothed normalized coverage bedgraphs in HoxD region
# Subtract viewpoints
# Extract segToFrag coverage from bigwig for mouse PT samples in HoxD region

./scripts/4C_02_subset_subtract.sh

# Compute cumsum of normalized score differences in mouse to define D-region borders.
# Transpose D-regions to chicken
# Quantify distribution of normalized score in mouse and chicken D-regions

Rscript scripts/4C_03_annotateDregions.R $workingDir/intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named_CNEs.bed "intermediateFiles/segToFrag.*E95.*bedgraph"

# Plot FigS3 A,B
pyGenomeTracks --tracks plots/figS3_4C_mPT.ini -o savedOutputs/plots/figS3_4C_mPT.pdf --region chr2:73800000-75800000 --width 18.5 --dpi 600 
pyGenomeTracks --tracks plots/figS3_4C_cPT.ini -o savedOutputs/plots/figS3_4C_cPT.pdf --region chr7:15790000-16700000  --dpi 600 --width 18.5 --decreasingXAxis

#rm -r intermediateFiles
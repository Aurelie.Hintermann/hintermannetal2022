[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6554916.svg)](https://doi.org/10.5281/zenodo.6554916)

# scripts for Hintermann & al. 2022

All scripts necessary to build figures from raw data in Hintermann et al 2022.

The analyses rely on a conda environment. To create it use:

```bash
conda env create -f HintermannEtAl2022.yml
```

The analyses also rely on R version 3.6.1.

*plot_all.sh* will get files, run bash and/or R scripts found in *scripts* and plot the data as in the Figures of the paper.

*files* contains custom annotation files and files downloawded from the UCSC genome browser. These latter files include chromosome sizes files, TSS annotation files and conservation files.

*plots* contains .ini files used with pygenomtracks (Ramírez et al. 2018; Lopez-Delisle et al. 2021) to obtain the figures as used in the paper before processing with illustator. 

*savedOutputs* contains the output files required in .ini files as well as the final pdf obtained.

Finally, *galaxyWorkflows* contains the details of the analyses done on the Galaxy platform (Afgan et al., 2018).

Contact: aurelie.hintermann@unige.ch


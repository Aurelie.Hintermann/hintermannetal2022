#! /usr/bash

# The 4C scripts are designed to work on a server with a slurm scheduler.

####
# First create the conda environment to run 4C
# (Only the first time)
####
# Start at the root of the gitDirectory
gitDirectory=$PWD/
conda env create -f ${gitDirectory}/4C/4C.yml
conda activate 4C

# I need libtsnnls:
# For the moment I did not find a better solution:
cd $(mktemp -d)
wget http://www.jasoncantarella.com/downloads/libtsnnls-2.3.4.tar.gz
tar xzf libtsnnls-2.3.4.tar.gz
cd libtsnnls-2.3.4/
module load gcc/8.4.0  mvapich2/2.3.4 openblas/0.3.10 netlib-scalapack/2.1.0
blas=$(ls $OPENBLAS_ROOT/lib/libopenblas*-r*.so)
./configure --with-blas=$blas --prefix=$CONDA_PREFIX
make install
cd ..

# Install python packages from github:
git clone https://github.com/bbcf/bbcflib.git -b standalone ## 
cd bbcflib
python setup.py install --prefix=$CONDA_PREFIX
cd .. 

git clone https://github.com/bbcf/bsPlugins.git
cd bsPlugins
python setup.py install --prefix=$CONDA_PREFIX
cd ..

# I need libbam:
wget https://archives.fedoraproject.org/pub/archive/epel/7/x86_64/Packages/s/samtools-libs-0.1.19-7.el7.x86_64.rpm
rpm2cpio samtools-libs-0.1.19-7.el7.x86_64.rpm | cpio -idmv
mv ./usr/lib64/libbam.so.1 $CONDA_PREFIX/lib/libbam.so
mkdir -p $CONDA_PREFIX/bin/lib/
cp $CONDA_PREFIX/lib/libbam.so $CONDA_PREFIX/bin/lib/libbam.so.1

conda deactivate
####
# Then get files:
####
# Get BBCFutils scripts:
pathForBBCFutils="/scratch/ldelisle/softwares/"
mkdir -p ${pathForBBCFutils}
cd ${pathForBBCFutils}
git clone https://github.com/bbcf/bbcfutils.git -b standalone

genomeDirectory="/scratch/ldelisle/genome/"
FourCDirectory="/scratch/ldelisle/4C/"
FourCAnalysis="analysis_Hintermann2022"

# Check that the sra table is available:
if [ ! -e ${gitDirectory}/4C/sraTable.txt ]; then
  echo "sraTable.txt does not exists."
  exit 1
fi

# First we need to create all the directories
mkdir -p ${FourCDirectory}

# To prepare the mapseq step:
# Update the init_bein_minilims.py
cp $gitDirectory/scripts/4C/init_bein_minilims.py ${FourCDirectory}
sed -i "s#/mnt/BBCF-raw#${FourCDirectory}#g" ${FourCDirectory}/init_bein_minilims.py
sed -i "s#/home/leleu/htsstation#${pathForBBCFutils}#g" ${FourCDirectory}/init_bein_minilims.py

####
# Prepare the genomes:
####

# For mm10
# Everything was precomputed when HTS station
# Was existing.
# I am using this.
cd ${FourCDirectory}
wget https://zenodo.org/record/5905346/files/minimal_HTSstation_mm10.tar?download=1 -O minimal_HTSstation_mm10.tar
tar xvmf minimal_HTSstation_mm10.tar

# For galGal6
# First I need a simplified version of the fasta:
mkdir -p ${genomeDirectory}/galGal6_simplified
wget https://hgdownload.soe.ucsc.edu/goldenPath/galGal6/bigZips/galGal6.fa.gz -P ${genomeDirectory}/galGal6_simplified/
chrToKeep="chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chr23,chr24,chr25,chr26,chr27,chr28,chr30,chr31,chr32,chr33,chrM,chrW,chrZ"
zcat ${genomeDirectory}/galGal6_simplified/galGal6.fa | awk -v chrs=$chrToKeep '
BEGIN{
    # Create an array with chrom as names
    split(chrs,mychrs,",")
    for (i in mychrs){
        myChrsAsKeys[">"mychrs[i]] = ""
    }
    # Initialize toPrint to False
    toPrint="F"
}
$0~/^>/{
    if($0 in myChrsAsKeys){
        toPrint="T"
    } else {
        toPrint="F"
    }
}
{
    if(toPrint=="T"){
        print
    }
}' > ${genomeDirectory}/galGal6_simplified/galGal6_simplified.fa

# Then I need to get from UCSC the repeat masker
# UCSC website in tools table browser variations and repeat.
# and put it in the good directory
cp galGal6_rmsk.bed ${genomeDirectory}/galGal6_simplified/galGal6_simplified_rmsk.bed

# Now I run the slurm script which will
# create the bowtie2 index and 
# json files and
# 4C libraries files
sbatch ${gitDirectory}/4C/slurm_scripts/00_prepareGenomesFor4C.sh ${gitDirectory} ${genomeDirectory}

####
# Get demultiplexed fastqs from SRA:
####

jidGetFastq=$(sbatch --chdir $FourCDirectory ${gitHubDirectory}/4C/slurm_scripts/01_getDemultiplexedFastq.sh $FourCAnalysis $gitDirectory | awk '{print $NF}')

####
# Run the mapping:
####

jidMapping4C=$(sbatch --chdir $FourCDirectory --dependency afterok:${jidGetFastq} ${gitDirectory}/4C/slurm_scripts/02_mapping.sh $FourCAnalysis $gitDirectory $genomeDirectory ${pathForBBCFutils} | awk '{print $NF}')

####
# Prepare the 4C config files:
####

jidpre4C=$(sbatch --chdir $FourCDirectory --dependency afterok:${jidMapping4C} ${gitDirectory}/4C/slurm_scripts/03_prepare4C.sh $FourCAnalysis $gitDirectory $genomeDirectory | awk '{print $NF}')

####
# Run the 4C:
####

sbatch --chdir $FourCDirectory --dependency afterok:${jidpre4C} ${gitDirectory}/4C/slurm_scripts/04_4Cseq.sh $FourCAnalysis $gitDirectory ${pathForBBCFutils}


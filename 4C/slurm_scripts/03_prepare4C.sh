#!/bin/bash

#SBATCH -o slurm-%x-%A.out
#SBATCH -e slurm-%x-%A.err
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=lucille.delisle@epfl.ch
#SBATCH --nodes 1
#SBATCH --mem 1G
#SBATCH --cpus-per-task 1
#SBATCH --time 1:00:00
#SBATCH --job-name pre4C

desc=$1
gitDirectory=$2
pathWithGenome=$3
# Here there is no br file
# BR files are designed for mutant genomes
pathWithInstall="$PWD/"
pathWithTableWithGenomes="${gitDirectory}/4C/genome_table.txt"
pathForScripts="${gitDirectory}/scripts/4C/"
pathWithBRFiles="${pathForScripts}/"
pathWithtemplate4cfile="${gitDirectory}/4C/template_4cfile.fa"


module purge

source $(dirname $(dirname $(which conda)))/etc/profile.d/conda.sh
conda activate 4C

wd="${pathWithInstall}/${desc}"
if [ ! -e ${wd} ]; then
  echo "${wd} does not exists"
  exit 1
fi
cd ${wd}

# Count the number of genomes in the table
if [ ! -e ${pathWithTableWithGenomes} ]; then
  echo "${pathWithTableWithGenomes} does not exists"
  exit 1
fi
n=$(cat ${pathWithTableWithGenomes} | awk 'END{print NR}')
# For each genome
for ((i=1;i<=${n};i++)); do
  genome=$(cat ${pathWithTableWithGenomes} | awk -v i=${i} 'NR==i{print $1}')
  brFiles=$(cat ${pathWithTableWithGenomes} | awk -v i=${i} 'NR==i{print $2}')
  if [ -z ${brFiles} ]; then
    assembly=${genome}
    if [ ${genome} = "mm10" ]; then
      genome="Wt"
    fi
  else
    assembly=mm10_${genome}
  fi
  if [ -e $PWD/res_files_mapping_${genome} ]; then
    # If there are mapping data:
    echo "preparing ${assembly}"
    # Copy the template with a name which matches the genome:
    if [ ! -e ${pathWithtemplate4cfile} ]; then
      echo "${pathWithtemplate4cfile} does not exists"
      exit 1
    fi
    cp ${pathWithtemplate4cfile} template_4cfile_${genome}.fa
    if [ ! -z ${brFiles} ]; then
      # We need to shift the coordinates which were in the template
      if [ ! -e ${pathForScripts}/shiftPrimerFileWithMultipleBR_poolMultipleVP.sh ]; then
        echo "${pathForScripts}/shiftPrimerFileWithMultipleBR_poolMultipleVP.sh does not exists"
        exit 1
      fi      
      bash ${pathForScripts}/shiftPrimerFileWithMultipleBR_poolMultipleVP.sh ${brFiles} template_4cfile_${genome}.fa ${pathWithBRFiles} ${pathForScripts}
      # The new template_4cfile_${genome}.fa has the coordinates which match the genome
    fi
    # To run the 4c pipline the header of the 4c primer file needs to match each sample
    # We also need a configuration file which specifies how replicates are merged together and where are the bam files.
    if [ ! -e ${pathForScripts}/build_config_4Cseq_5Mb_and_primerfile_withReplicates_individual_merge.sh ]; then
      echo "${pathForScripts}/build_config_4Cseq_5Mb_and_primerfile_withReplicates_individual_merge.sh does not exists"
      exit 1
    fi    
    bash ${pathForScripts}/build_config_4Cseq_5Mb_and_primerfile_withReplicates_individual_merge.sh $PWD ${genome} ${assembly} ${pathWithGenome} template_4cfile_${genome}.fa
    if [ "$pathWithGenome" = "./" ]; then
      for file in config_4cseq_${genome}_split*.txt; do
        sed -i "s#TODO#${pathWithInstall}/4cLibraries/${assembly}/library_${assembly}_Nla_Dpn_30bps_segmentInfos.bed.gz#" $file
      done
    fi
    if [ "$assembly" = "mm10" ] && [ ! -e "${pathWithGenome}/${genome}/library_${genome}_NlaDpn_30bps_segmentInfos.bed" ]; then
      for file in config_4cseq_${genome}_split*.txt; do
        sed -i "s#${pathWithGenome}/${genome}/library_${genome}_NlaDpn_30bps_segmentInfos.bed#${pathWithInstall}/4cLibraries/${assembly}/library_${assembly}_Nla_Dpn_30bps_segmentInfos.bed.gz#" $file
      done
    fi
  fi
done

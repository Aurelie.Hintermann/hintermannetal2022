#!/bin/bash

originalGtf=(Mus_musculus.GRCm38.99 Gallus_gallus.GRCg6a.99)
chr=(chr2 chr7)
Hx=(Hox HOX)
wget "https://raw.githubusercontent.com/lldelisle/scriptsForFernandezGuerreroEtAl2020/e399194b40999f8fd24c4370561813844039150a/RNAseq/fromgtfTobed12.py" -P downloads -nc

if [ $1 == ensembl ]; then

    echo "Download gtf files from Ensembl and make custom gtf files."

    wget "https://raw.githubusercontent.com/lldelisle/scriptsForFernandezGuerreroEtAl2020/e399194b40999f8fd24c4370561813844039150a/RNAseq/filterGtfLikeAnouk.R" -P downloads -nc
    wget "https://raw.githubusercontent.com/lldelisle/toolBoxForMutantAndWTGenomes/5368aecec9b406d8e29f22331a3d7e51b6c4e5c8/scripts/mergeGenes_overlap.R" -P downloads -nc
    wget "ftp://ftp.ensembl.org/pub/release-99/gtf/mus_musculus/Mus_musculus.GRCm38.99.gtf.gz" -P downloads -nc
    wget "ftp://ftp.ensembl.org/pub/release-99/gtf/gallus_gallus/Gallus_gallus.GRCg6a.99.gtf.gz" -P downloads -nc

    for i in ${!originalGtf[*]}; do 

        gtfFile=downloads/"${originalGtf[$i]}".gtf.gz
        echo "$gtfFile"
        gunzip -k "$gtfFile"
        gtfFile=${gtfFile/.gtf.gz/.gtf}
        shName=$(basename "$gtfFile" .gtf)

        # adapted from https://github.com/lldelisle/scriptsForFernandezGuerreroEtAl2020/blob/master/RNAseq/00_gtf_filtering.sh
        # Filter for read-through transcripts and non-coding transcripts in coding genes.
        Rscript downloads/filterGtfLikeAnouk.R "$gtfFile" 

        # Merge genes with same name which overlap
        Rscript downloads/mergeGenes_overlap.R downloads/FilteredTranscriptsOf"$shName"_ExonsOnly_UCSC.gtf

        # adapted from https://github.com/lldelisle/scriptsForAmandioEtAl2021/blob/103d950ddc8b92012295a728c58c95b19cecec68/plots/01_plots.sh
        # From gtf deduce a simplified gene annotations
        grep 'transcript_biotype "protein_coding"' downloads/FilteredTranscriptsOf"$shName"_ExonsOnly_UCSC.gtf | grep -v "Hoxa3-201" | grep -v "Hoxd3-203" | grep -v "HOXA3-203" > downloads/"$shName"_simplified.gtf
        gzip downloads/"$shName"_simplified.gtf
        rm "$gtfFile"
        done

    rm downloads/*_UCSC.*
    rm downloads/ReadthroughTranscripts*

elif [ $1 == custom ]; then
    echo "Download custom gtf files from Zenodo."

    wget "https://zenodo.org/record/5917676/files/Mus_musculus.GRCm38.99_simplified.gtf.gz?download=1" -P downloads -O downloads/Mus_musculus.GRCm38.99_simplified.gtf.gz -nc
    wget "https://zenodo.org/record/5917720/files/Gallus_gallus.GRCg6a.99_simplified.gtf.gz?download=1" -P downloads -O downloads/Gallus_gallus.GRCg6a.99_simplified.gtf.gz -nc

else
    echo "startingGTF is not properly defined."
fi

for i in ${!originalGtf[*]}; do 
    gtfFile=downloads/"${originalGtf[$i]}"_simplified.gtf.gz
    echo "$gtfFile"
    gunzip -k "$gtfFile"
    gtfFile=${gtfFile/.gtf.gz/.gtf}
    shName=$(basename "$gtfFile" .gtf)

    cat $gtfFile | awk -v CHR="${chr[$i]}" '$1==CHR {print}' > intermediateFiles/"$shName"_"${chr[$i]}".gtf

    # Split the gtf in Hox and non Hox:
    grep "${Hx[$i]}" intermediateFiles/"$shName"_"${chr[$i]}".gtf > intermediateFiles/"$shName"_"${chr[$i]}"_Hox.gtf
    grep -v "${Hx[$i]}" intermediateFiles/"$shName"_"${chr[$i]}".gtf > intermediateFiles/"$shName"_"${chr[$i]}"_nonHox.gtf

    # Make a bed to display HoxD as a rectangle 
    # Find smallest start coordinate
    startC=$(sort -nk 4 intermediateFiles/"$shName"_"${chr[$i]}"_Hox.gtf | head -n 1 | cut -f 4)
    # Find biggest end coordinate
    endC=$(sort -r -nk 5 intermediateFiles/"$shName"_"${chr[$i]}"_Hox.gtf | head -n 1 | cut -f 5)
    #write bed file 
    echo -e "${chr[$i]}\t${startC}\t${endC}\tHoxD" > intermediateFiles/"$shName"_"${chr[$i]}"_Hox.bed
    rm downloads/"$shName".gtf
done 

python downloads/fromgtfTobed12.py --output intermediateFiles/Mus_musculus.GRCm38.99_simplified_chr2_nonHox_12col.bed --mergeTranscriptsAndOverlappingExons intermediateFiles/Mus_musculus.GRCm38.99_simplified_chr2_nonHox.gtf
awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4,$5,$6}' intermediateFiles/Mus_musculus.GRCm38.99_simplified_chr2_nonHox_12col.bed > intermediateFiles/Mus_musculus.GRCm38.99_simplified_chr2_nonHox.bed

python downloads/fromgtfTobed12.py --output intermediateFiles/Gallus_gallus.GRCg6a.99_simplified_chr7_nonHox_12col.bed --mergeTranscriptsAndOverlappingExons intermediateFiles/Gallus_gallus.GRCg6a.99_simplified_chr7_nonHox.gtf
awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4,$5,$6}' intermediateFiles/Gallus_gallus.GRCg6a.99_simplified_chr7_nonHox_12col.bed > intermediateFiles/Gallus_gallus.GRCg6a.99_simplified_chr7_nonHox.bed

cp intermediateFiles/*[xl].bed savedOutputs/files/
cp downloads/*simplified.gtf.gz savedOutputs/files/
gunzip savedOutputs/files/*.gz
rm intermediateFiles/*

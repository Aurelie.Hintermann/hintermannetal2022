import os
import sys
import time
import pyBigWig


def diffCoverage(bw1, bw2, chr, start_pos, end_pos, step):
    coverage1 = bw1.stats(chr, start=start_pos, end=end_pos, nBins=(end_pos - start_pos) // step, type="sum", exact=True)
    coverage2 = bw2.stats(chr, start=start_pos, end=end_pos, nBins=(end_pos - start_pos) // step, type="sum", exact=True)
    # Replace None by 0:
    coverage1 = [c if c is not None else 0 for c in coverage1]
    coverage2 = [c if c is not None else 0 for c in coverage2]
    # Perform the diff with no negative values:
    return([max(0, c1 - c2) for c1, c2 in zip(coverage1, coverage2)])


def scanFromStartPos(bw1, bw2, chr, start_pos, end_pos, thr, step):
    '''
    This function will return the smaller region
    starting at chr:start_pos which is n * step long
    whose sum in bw is over thr
    If it cannot it will raise an Exception.
    '''
    for n in range(1, (end_pos - start_pos) // step):
        current_end = start_pos + n * step
        sum_coverage = sum(diffCoverage(bw1, bw2, chr, start_pos, current_end, step))
        if sum_coverage >= thr:
            return(chr + "\t" + str(start_pos) + "\t"
                   + str(current_end) + "\t" + str(sum_coverage)+ "\n")
    raise Exception("Could not find when scanning from : " + str(start_pos))


def scanSlWindow(bw_file1, bw_file2, chr, start_pos, end_pos, thr_prop, step_start, step_length):
    '''
    This function will scan starting
    from every step_start fragments
    to look for the region which is n * step_length
    which is the smaller and
    whose coverage of (bw_file1 - bw_file2) is above the fraction thr_prop
    '''
    bw1 = pyBigWig.open(bw_file1)
    bw2 = pyBigWig.open(bw_file2)
    total_coverage = sum(diffCoverage(bw1, bw2, chr, start_pos, end_pos, step_length))
    thr = total_coverage * thr_prop
    print("\nThreshold to reach: " + str(thr_prop) +"\nCoverage to reach: " + str(thr))
    best_region = ""
    best_region_length = end_pos - start_pos
    for current_start in range(start_pos, end_pos, step_start):
        try:
            line = scanFromStartPos(bw1, bw2, chr, current_start, end_pos, thr, step_length)
            current_end = int(line.strip().split("\t")[2])
            if (current_end - current_start) < best_region_length:
                best_region = line
                best_region_length = current_end - current_start
        except Exception as e:
            print(e)
            break
    outputName2=bw_file1.split("/")[len(bw_file1.split("/"))-1].split(".")[0]+"_scanned0"+str(thr_prop).split(".")[1]+"_"+str(step_start)+"_"+str(step_length)+"_MAR.bed"
    print("\n\Output file: "+outputName2)
    with open(outputName2, "w") as fo:
        ls = best_region.strip().split("\t")
        fo.write("\t".join(ls[:3]))
    scannedRegion_name = "regionForWindows_" + chr + ".bed"
    print("\n\nScanned region coordinates: "+scannedRegion_name     )
    with open(scannedRegion_name, "w") as wf:
        scanReg = chr + "\t" + str(start_pos) + "\t" + str(end_pos) + "\t" + "coverageScannedRegion"
        wf.write(scanReg)

if __name__ == "__main__":
    # Usage is python scanRegionForHighestDensity.py bigwig_fileOfInterest bigwig_fileRef chrom start end threshold_proportion step_start step_length
    print(sys.argv[1])
    scanSlWindow(sys.argv[1], sys.argv[2], sys.argv[3],
                 int(sys.argv[4]), int(sys.argv[5]), float(sys.argv[6]),
                 int(sys.argv[7]), int(sys.argv[8]))

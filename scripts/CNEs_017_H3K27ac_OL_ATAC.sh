#!/bin/bash


# Get ATAC peaks for each condition by the union of peaks of the two replicates. 
mkdir -p intermediateFiles/ATAC

for rep1 in downloads/ATAC/*rep1*.narrowPeak; do
    condition=$(basename $rep1 _rep1)
    condition=${condition/_rep1.*/}
    rep2=${rep1/rep1/rep2}

    # get genome
    if [[ $condition =~ _HH ]]; then
        # Chicken stage
        genome=files/galGal6.chrom.sizes
    elif [[ $condition =~ _E[0-9] ]]; then
        # Mouse stage
        genome=files/mm10.chrom.sizes
    fi

    # Merge of replicates:  compute peak union, extend each peak by 660 bp both sides and merge. 
    cat $rep1 $rep2 | bedtools sort -i /dev/stdin -g $genome | bedtools slop -b 660 -i /dev/stdin -g $genome | bedtools merge -i /dev/stdin | uniq > intermediateFiles/ATAC/${condition}_neq2.bed
done

# Get H3K27ac narrowPeaks which overlap ATAC peaks in matching tissues
mkdir -p intermediateFiles/narrowPeakOL_ATAC_forCNEs_gg6
mkdir -p intermediateFiles/narrowPeakOL_ATAC_forCNEs_mm10

ChIPfiles=("E125_FB_rep1" "E125_WP_rep1" "E85_PT_rep1rep2" "HH35_FB_rep1" "HH35_DS_rep1rep2" "HH18_PT_rep1rep2")
ATACfiles=("E125_FB" "E125_WP" "E95_PT" "HH35_FB" "HH35_DS" "HH19_PT")

echo -e "ChIP_file\tATAC_file\tOL_file\tChIP_peakCount\tOL_peakCount\tOL_on_ChIP_fraction" > savedOutputs/files/ATAC_quantif_ChIP_OL.txt

for i in "${!ChIPfiles[@]}"; do
    ChIP=downloads/ChIP/ChIP_H3K27ac_${ChIPfiles[$i]}.narrowPeak

    if [[ $ChIP =~ _HH ]]; then
        genome=files/galGal6.chrom.sizes
        sp="gg6"
    elif [[ $ChIP =~ _E[0-9] ]]; then
        genome=files/mm10.chrom.sizes
        sp="mm10"
    fi

    ChIPname=ChIP_H3K27ac_${ChIPfiles[$i]}

    ATAC=intermediateFiles/ATAC/ATAC_${ATACfiles[$i]}_neq2.bed
    ATACname=ATAC_${ATACfiles[$i]}

    # find ChIP peaks that overlap with ATAC peaks
    bedtools intersect -wa -u -a "$ChIP" -b "$ATAC" | bedtools sort -i /dev/stdin -g $genome > savedOutputs/files/${ChIPname}_OL_${ATACname}.narrowPeak
    cp savedOutputs/files/${ChIPname}_OL_${ATACname}.narrowPeak intermediateFiles/narrowPeakOL_ATAC_forCNEs_${sp}/${ChIPname}_OL_${ATACname}.narrowPeak

    # count how many ChIP peaks overlap with ATAC peaks
    nbPeaksOL=$(cut -f1-3 < savedOutputs/files/${ChIPname}_OL_${ATACname}.narrowPeak | uniq | wc -l)

    # count how many ChIP peaks
    cut -f1-3 $ChIP | uniq > tmp.txt
    nbPeaksChIP=$(wc -l < tmp.txt)

    # calculate the proportion of ChIP peaks OL / tot
    fract=$(echo "scale = 2; $nbPeaksOL / $nbPeaksChIP" | bc)

    #write in output file
    echo -e "$ChIPname\t$ATACname\t${ChIPname}_OL_${ATACname}\t$nbPeaksChIP\t$nbPeaksOL\t$fract" >> savedOutputs/files/ATAC_quantif_ChIP_OL.txt
done

echo -e "\n New quantification file created: savedOutputs/files/ATAC_quantif_ChIP_OL.txt \n"

#cp savedOutputs/files/ChIP_H3K27ac_HH*_OL_ATAC_*.narrowPeak intermediateFiles/narrowPeakOL_ATAC_forCNEs_gg6/
#cp savedOutputs/files/ChIP_H3K27ac_E*_OL_ATAC_*.narrowPeak intermediateFiles/narrowPeakOL_ATAC_forCNEs_mm10/

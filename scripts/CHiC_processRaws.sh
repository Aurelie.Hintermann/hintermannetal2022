#!/bin/bash

mkdir -p downloads/CHiC

# Download raw cool files
getFromGEO GSM5835528_CHiC_E95_PT_5kb_raw.cool.gz downloads/CHiC/
getFromGEO GSM5835529_CHiC_HH18_PT_2half_raw.cool.gz downloads/CHiC/

# Gunzip them
gunzip -k downloads/CHiC/*cool.gz

## Mouse

# diagnostic plot and correction
min=$(hicCorrectMatrix diagnostic_plot -m downloads/CHiC/CHiC_E95_PT_5kb_raw.cool --chromosomes chr2 -o intermediateFiles/CHiC_E95_PT_5kb_raw_chr2.diag.png 2>&1 >/dev/null | awk 'NR==2{print $NF}')
hicCorrectMatrix correct -m downloads/CHiC/CHiC_E95_PT_5kb_raw.cool --correctionMethod ICE -t "$min" 5 --chromosomes chr2 -o intermediateFiles/CHiC_E95_PT_5kb_iced_chr2_toCorrect_tmp.cool

# make pyGenomeTracks read corrected values instead of raws
# This is a bug present in hicexplorer 3.7.2
# See https://github.com/deeptools/HiCExplorer/issues/586
hicConvertFormat -m intermediateFiles/CHiC_E95_PT_5kb_iced_chr2_toCorrect_tmp.cool -o intermediateFiles/CHiC_E95_PT_5kb_iced_chr2.cool --inputFormat cool --outputFormat cool --load_raw_values

# TAD calling
hicFindTADs --matrix intermediateFiles/CHiC_E95_PT_5kb_iced_chr2.cool --outPrefix intermediateFiles/CHiC_E95_PT_5kb_iced_chr2 --correctForMultipleTesting fdr --minDepth 240000 --maxDepth 480000 --step 480000 --minBoundaryDistance 200000

## Same for chicken 
# Except that the parameters of TAD calling are divided by 2
min=$(hicCorrectMatrix diagnostic_plot -m downloads/CHiC/CHiC_HH18_PT_2half_raw.cool --chromosomes chr7  -o intermediateFiles/CHiC_HH18_PT_2half_raw.diag.png 2>&1 >/dev/null | awk 'NR==2{print $NF}')
hicCorrectMatrix correct -m downloads/CHiC/CHiC_HH18_PT_2half_raw.cool --correctionMethod ICE -t "$min" 5 --chromosomes chr7 -o intermediateFiles/CHiC_HH18_PT_2half_iced_chr7_toCorrect_tmp.cool
hicConvertFormat -m intermediateFiles/CHiC_HH18_PT_2half_iced_chr7_toCorrect_tmp.cool -o intermediateFiles/CHiC_HH18_PT_2half_iced_chr7.cool --inputFormat cool --outputFormat cool --load_raw_values
hicFindTADs --matrix intermediateFiles/CHiC_HH18_PT_2half_iced_chr7.cool --outPrefix intermediateFiles/CHiC_HH18_PT_2half_iced_chr7 --correctForMultipleTesting fdr --minDepth 120000 --maxDepth 240000 --step 240000 --minBoundaryDistance 150000

cp intermediateFiles/CHiC_*_iced_chr[27].cool savedOutputs/files/
cp intermediateFiles/*chr[27]_domains.bed savedOutputs/files/
rm intermediateFiles/*

import os
import sys
import time
import pyBigWig


def normedCoverage(bw_file, nbReadsInPeakFile, ws, chr, start_pos, end_pos):
    """
    Returns normalized coverage for the considered region
    Normalization factor is in nbReadsInPeakFile file

    Args:
        bw_file (string): file path to bigwig file
        nbReadsInPeakFile (string): file path with 
                                    first column with sample name
                                    second column with value to use for normalization
        ws (int): window size on which coverage is computed
        chr (string): chromosome name
        start_pos (int): start position
        end_pos (int): end position
    """
    with pyBigWig.open(bw_file) as bw:
        # Get the sample name
        shortName=bw_file.split("ATAC_")[1].split(".bigwig")[0]
        # Get the coverage
        coverage = bw.stats(chr, start=start_pos, end=end_pos,nBins=(end_pos - start_pos) // ws, type="mean", exact=True)
        # Substitute None by 0
        coverage = [c if c is not None else 0 for c in coverage]
        # Get the scaling factor:
        with open(nbReadsInPeakFile) as f:
            for line in f:
                if shortName in line:
                    scaleF=int(line.split("\t")[1])/1000000
                    print("Scaling factor for sample " + shortName + " is " + str(scaleF) + "\n")
    return([round(c/scaleF,2) for c in coverage])


def AvgRepWithNormedCoverage(bw_rep1, bw_rep2, nbReadsInPeakFile, ws, outputFile, chr, start_pos, end_pos):
    """
    Compute the average between 2 bigwig files on a specific region
    for a specific window size and write output in a bedgraph

    Args:
        bw_rep1 (string): file path to first bigwig file
        bw_rep2 (string): file path to second bigwig file
        nbReadsInPeakFile (string): file path with 
                                    first column with sample name
                                    second column with value to use for normalization
        ws (int): window size on which coverage is computed
        outputFile (string): file path to output bedgraph
        chr (string): chromosome name
        start_pos (int): start position
        end_pos (int): end position
    """

    # Get the coverage for each bigwig file
    normedCovRep1=normedCoverage(bw_rep1, nbReadsInPeakFile, ws, chr, start_pos, end_pos)
    normedCovRep2=normedCoverage(bw_rep2, nbReadsInPeakFile, ws, chr, start_pos, end_pos)

    # Compute the average
    averagedCov=[round((c1 + c2)/2,2) for c1, c2 in zip(normedCovRep1, normedCovRep2)]
    
    o_startPos=list(range(start_pos,end_pos,ws))
    o_endPos=list(range(start_pos+ws,end_pos+ws,ws))
    
    with open(outputFile, "w") as o_bg:
        for i in range(len(averagedCov)):
            o_bg.write(chr + "\t" + str(o_startPos[i]) + "\t" + str(o_endPos[i]) + "\t" + str(averagedCov[i]) + "\n")
    return()

if __name__ == "__main__":
    # Usage is python normBigWig.py workingDir
    directory=sys.argv[1]
    m_conditions = ["E95_PT", "E125_WP", "E125_FB"]
    [AvgRepWithNormedCoverage(directory + "/downloads/ATAC/ATAC_" + c + "_rep1.bigwig", directory + "/downloads/ATAC/ATAC_" + c + "_rep2.bigwig", directory + "/files/ATAC_readsInPeaks.tsv", 20, directory + "/savedOutputs/files/ATAC_" + c + "_HoxD_neq2.bedgraph", "chr2", 73000000, 77000000) for c in m_conditions]
    c_conditions = ["HH19_PT", "HH35_FB", "HH35_DS"]
    [AvgRepWithNormedCoverage(directory + "/downloads/ATAC/ATAC_" + c + "_rep1.bigwig", directory + "/downloads/ATAC/ATAC_" + c + "_rep2.bigwig", directory + "/files/ATAC_readsInPeaks.tsv", 20, directory + "/savedOutputs/files/ATAC_" + c + "_HOXD_neq2.bedgraph", "chr7", 13000000, 17000000) for c in c_conditions]

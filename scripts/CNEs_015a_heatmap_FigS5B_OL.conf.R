#####
### config file for Pheatmap
#####

# Path to file for matrix
inData <- "intermediateFiles/gw_mm_gg_CNEs_activeInAtLeastOne_forMx_OL_ATAC.txt"

generalName <- "savedOutputs/plots/figS5BOL"

samplesToPlot <- c()
remFB <- FALSE

matrixColumns <- "CNE_name" # CNEs
matrixRows <- "H3K27ac_sampleName" # samples
matrixValues <- "H3K27ac_peakValue" # Scores

# color scale. Put NA for automatic color scale.
# To contrast the plots more, decrease the max value
 customBreaks <- seq(0, 7, by = .7)

# Number of different colors you have on the color scale
# Specify only if you use automatic scale. 
col_len <- 20
if(!unique(is.na(customBreaks))){
  col_len <- length(customBreaks)
}

# clustering method
c_method <- "ward.D2"

# clustering distance for rows
row_dist <- "spearman" #"correlation"  #
#clustering distance for cols
col_dist <- "spearman" #  "correlation"  # 

# If you want to annotate clusters of columns, put T
annotateRandC <- F
nbOfClusters <- 2
nbOfClusters_r <- 2

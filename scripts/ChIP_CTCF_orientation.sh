#!/bin/bash


# CTCF orientation annotations were obtained using the motif prediction tables avalaible as:
# files/ChIP_CTCF_E105_PT_rep1_plotted_extended_insdb_output.txt and files/ChIP_CTCF_HH18_PT_rep1_plotted_extended_insdb_output.txt for mouse and chicken respectively.

# The steps to generate these tables are shown below. 

generateNewMotifPrediction=no

### Mouse CTCF peaks

if [ $generateNewMotifPrediction == yes ]; then

# Get the chr2 fasta:
    if [ ! -e files/chr2.fa ]; then
        wget https://hgdownload.soe.ucsc.edu/goldenPath/mm10/chromosomes/chr2.fa.gz -P files/ -nc
        gunzip files/chr2.fa.gz
    fi
    # Get CTCF peaks
    getFromGEO GSM5835468_ChIP_CTCF_E105_PT_rep1.narrowPeak.gz downloads/ChIP/
    gunzip -k downloads/ChIP/GSM5835468_ChIP_CTCF_E105_PT_rep1.narrowPeak.gz
    # The peaks of ChIP_CTCF_E105_PT_rep1.bed are extended 100bp each direction
    cat downloads/ChIP/ChIP_CTCF_E105_PT_rep1.narrowPeak | awk -v OFS="\t" '$1=="chr2" && $2 < 75800320 && $3 > 73700000 {print $1, $2 - 100, $3 + 100, $4}' > intermediateFiles/ChIP_CTCF_E105_PT_rep1_plotted_extended.bed
    # The fasta is extracted
    bedtools getfasta -fi files/chr2.fa -bed intermediateFiles/ChIP_CTCF_E105_PT_rep1_plotted_extended.bed > intermediateFiles/ChIP_CTCF_E105_PT_rep1_plotted_extended.fa

    printf "Upload ChIP_CTCF_E105_PT_rep1_plotted_extended.fa on the website of http://insulatordb.uthsc.edu in CTCFBS Prediction Tool.\n"
    read -p "Save table output for MIT_LM7 as files/ChIP_CTCF_E105_PT_rep1_plotted_extended_insdb_output.txt and press any key to resume."

fi 

OUTPUT=files/ChIP_CTCF_E105_PT_rep1_plotted_extended_insdb_output.txt
# The orientation of the CTCF motif is deduced and put to . if score is less than 0.
cat $OUTPUT | awk -v OFS="\t" '{split($3, a, ":|-"); if($7 < 0){$6=".";$7=".";$8="."}; print a[1], a[2] + $4 + $5/2, a[2] + $4 + $5/2 + 1, $3, $7, $6}' > intermediateFiles/ChIP_CTCF_E105_PT_rep1.bed
# Plus and minus sites are written in separate files
grep "+$" intermediateFiles/ChIP_CTCF_E105_PT_rep1.bed > savedOutputs/files/ChIP_CTCF_E105_PT_rep1_plus.bed
grep "\-$" intermediateFiles/ChIP_CTCF_E105_PT_rep1.bed > savedOutputs/files/ChIP_CTCF_E105_PT_rep1_minus.bed

### Chicken CTCF peaks

if [ $generateNewMotifPrediction == yes ]; then

    # Get the galGal6 fasta:
    if [ ! -e files/galGal6.fa ]; then
        wget https://hgdownload.soe.ucsc.edu/goldenPath/galGal6/bigZips/galGal6.fa.gz -P files/ -nc
        gunzip files/galGal6.fa.gz
    fi
    #wget https://XXX/ChIP_CTCF_HH18_PT_rep1.narrowPeak -P downloads/ChIP -O ChIP_CTCF_HH18_PT_rep1.narrowPeak -nc

    cat downloads/ChIP/ChIP_CTCF_HH18_PT_rep1.narrowPeak | awk -v OFS="\t" '$1=="chr7" && $2 < 19000000 && $3 > 13000000 {print $1, $2 - 100, $3 + 100, $4}' > intermediateFiles/ChIP_CTCF_HH18_PT_rep1_plotted_extended.bed
    bedtools getfasta -fi files/galGal6.fa -bed intermediateFiles/ChIP_CTCF_HH18_PT_rep1_plotted_extended.bed > intermediateFiles/ChIP_CTCF_HH18_PT_rep1_plotted_extended.fa

    # Upload ChIP_CTCF_HH18_PT_rep1_plotted_extended.fa on the website of http://insulatordb.uthsc.edu/ in CTCFBS Prediction Tool.
    read -p "Save table output for MIT_LM7 as files/ChIP_CTCF_HH18_PT_rep1_plotted_extended_insdb_output.txt and press any key to resume."
fi

OUTPUT=files/ChIP_CTCF_HH18_PT_rep1_plotted_extended_insdb_output.txt
# The orientation of the CTCF motif is deduced and put to . if score is less than 0.
cat $OUTPUT | awk -v OFS="\t" '{split($3, a, ":|-"); if($7 < 0){$6=".";$7=".";$8="."}; print a[1], a[2] + $4 + $5/2, a[2] + $4 + $5/2 + 1, $3, $7, $6}' > intermediateFiles/ChIP_CTCF_HH18_PT_rep1.bed
# Plus and minus sites are written in separate files
grep "+$" intermediateFiles/ChIP_CTCF_HH18_PT_rep1.bed > savedOutputs/files/ChIP_CTCF_HH18_PT_rep1_plus.bed
grep "\-$" intermediateFiles/ChIP_CTCF_HH18_PT_rep1.bed > savedOutputs/files/ChIP_CTCF_HH18_PT_rep1_minus.bed

# Copy and clean outputs
rm intermediateFiles/*CTCF*

#!/bin/bash

mkdir -p intermediateFiles
printf "\n\nSubset normed smoothed bedgraphs in the HoxD region.\n"
printf "\n\nExtract segToFrag bedgraph from bigwig in region of interest.\n"

for f in downloads/4C/*bedgraph downloads/4C/*segToFrag* ; do
  filename=$(basename -- "$f")

  if [[ $f =~ _E[0-9] ]]; then

    chr="chr2"
    cStart=73500000
    cEnd=76000000

  elif [[ $f =~ _HH ]]; then
    chr="chr7"
    cStart=15700000
    cEnd=16800000
  fi

  if [[ $f =~ segToFrag ]]; then
    outputName=$(basename $f .bw)
    bigWigToBedGraph $f intermediateFiles/${outputName}_HoxD.bedgraph chrom="$chr" start="$cStart" end="$cEnd"
  
  else 
    awk -v chromo="$chr" -v cooS="$cStart" -v cooE="$cEnd" 'BEGIN { OFS = "\t" } {if(($1 == chromo) && ($2 > cooS ) && ($3 < cooE)){print $0}}' "$f" > savedOutputs/files/"${filename/.bedgraph/_HoxD.bedgraph}"
  
  fi
done

printf "\n\nSubtract viewpoints:\n"

vps=(1 4 9)
for n in Hoxd HOXD ; do
  for i in 0 1 ; do 
    f1=$(find savedOutputs/files/4C*PT*HoxD.bedgraph -type f -print | grep $n"${vps[$i]}" | grep -v minus)
    f2=$(find savedOutputs/files/4C*PT*HoxD.bedgraph -type f -print | grep $n"${vps[$((i+1))]}" | grep -v minus)
    outName=$(basename "$f1" .bedgraph)
    bedtools unionbedg -i "$f1" "$f2" > intermediateFiles/"$outName"_minusNextVP_tmp.bedgraph
    awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4-$5}' intermediateFiles/"$outName"_minusNextVP_tmp.bedgraph > savedOutputs/files/"$outName"_minusNextVP.bedgraph
    echo "$outName"_minusNextVP.bedgraph
    rm intermediateFiles/"$outName"_minusNextVP_tmp.bedgraph
  done
done


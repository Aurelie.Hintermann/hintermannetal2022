#!/bin/bash

######
### Functions
######

gtfToExonAndTSSbed () {
    # This function write a bed file with exons and TSS
    # With unique names
    # $1 simplified gtf (This file has only exons, see GTF.sh)
    # $2  = files/XXX_all_TSS_2kbminus500bpplus_collapse.bed
    # $3 is the genome name
    local genome=$3

    # make unique non-overlapping intervals with exons
    awk 'OFS = "\t" {print $1,$4,$5,substr($10, 2, length($10)-3)}' "$1" | uniq | bedtools sort -i "stdin" | bedtools merge -i "stdin" > intermediateFiles/tmp1.bed
    
    # Select intervals which overlap TSS and give unique name 
    bedtools intersect -a intermediateFiles/tmp1.bed -b "$2" -v > intermediateFiles/tmp2.bed
    awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,"exon_"NR}' intermediateFiles/tmp2.bed > intermediateFiles/"$(basename "$1" .gtf)".exons_intervals.bed
    
    # Combine and sort both bed files
    bedtools intersect -a intermediateFiles/tmp1.bed -b "$2" > intermediateFiles/tmp3.bed
    awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,"TSS_"NR}' intermediateFiles/tmp3.bed > intermediateFiles/"$(basename "$1" .gtf)".TSS_intervals.bed

    # Writing interval file containing exon and TSS intervals with unique names.
    cat intermediateFiles/"$(basename "$1" .gtf)".exons_intervals.bed intermediateFiles/"$(basename "$1" .gtf)".TSS_intervals.bed | uniq | bedtools sort -i "stdin" -g files/"$genome".chrom.sizes > intermediateFiles/"$(basename "$1" .gtf)".exonsAndTSS_intervals.bed
    echo "New file created: intermediateFiles/$(basename "$1" .gtf).exonsAndTSS_intervals.bed"
    
    # Cleaning temporary files.
    rm intermediateFiles/tmp1.bed intermediateFiles/tmp2.bed intermediateFiles/tmp3.bed intermediateFiles/"$(basename "$1" .gtf)".exons_intervals.bed intermediateFiles/"$(basename "$1" .gtf)".TSS_intervals.bed
}

generateGI () {
    # Generate genomic interval from several annotation files.

    local startingFolder="$1"
    local nameOfNewFile="$2"
    # $3 mm10 or galGal6

    # Create a new file a concatenation of all files of startingFolder
    # (usually peak files)
    echo "" > "$nameOfNewFile"_tmp1.bed
    for f in "$startingFolder"/*; do cat "$f" >> "$nameOfNewFile"_tmp1.bed ; done
    # Merge to have non-overlapping intervals this become a bed with 3 columns
    bedtools sort -i "$nameOfNewFile"_tmp1.bed -g files/"$3".chrom.sizes > "$nameOfNewFile"_tmp2.bed
    bedtools merge -i "$nameOfNewFile"_tmp2.bed > "$nameOfNewFile"_tmp3.bed
    # Add a new column with unique name
    awk -v colNam="$(basename "$nameOfNewFile")" 'BEGIN{OFS = "\t"}{print $0, colNam"_"NR}' "$nameOfNewFile"_tmp3.bed > "$nameOfNewFile"_tmp4.bed
    # Expand the coordinates 660bp each direction
    bedtools slop -b 660 -i "$nameOfNewFile"_tmp4.bed -g files/"$3".chrom.sizes > intermediateFiles/"$(basename "$nameOfNewFile")".bed
    rm "$nameOfNewFile"_tmp1.bed "$nameOfNewFile"_tmp2.bed "$nameOfNewFile"_tmp3.bed "$nameOfNewFile"_tmp4.bed
    echo "New file created: intermediateFiles/$(basename "$nameOfNewFile").bed"
}

annotateGI () {

    # Annotate previously generated genomic intervals with several annotation files.

    local fileName=$1 # The file to annotate
    # $2 is a directory with actylation peaks
    # $3 is a bed file with exons and TSS
    # $4 is a bed with the conserved sequences
    local genome=$5

    # acetylation peaks
    # Extend each peak by 660 bp both sides to account for nucleosome occupancy.
    if [ -e "$2"_ext660 ]; then
        rm -r "$2"_ext660
    fi
    mkdir -p "$2"_ext660
    for f in "$2"/*; do 
        bedtools slop -b 660 -i "$f" -g files/"$genome".chrom.sizes > "$2"_ext660/$(basename "$f" .narrowPeak)_ext660.narrowPeak
    done

    # Store the score of the acetylation peak and the name of the sample
    # -loj to report no overlap by "."
    bedtools intersect -wa -wb -a "$fileName" -b "$2"_ext660/* -filenames -sorted -loj -g files/"$genome".chrom.sizes | awk 'BEGIN{OFS = "\t"}($12 != "."){sub(".*/","",$4) ; sub(".*ChIP_H3K27","ChIP_H3K27",$5); sub(".narrowPeak","",$5); print $1,$2,$3,$4,$12,$5}' | uniq > intermediateFiles/"$(basename "$fileName" .bed)"_annotated_tmp0.txt
    # is Coding
    bedtools intersect -wa -wb -a intermediateFiles/"$(basename "$fileName" .bed)"_annotated_tmp0.txt -b "$3" -sorted -loj -f 0.01 -g files/"$genome".chrom.sizes | awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4,$5,$6,$10}' | uniq > "$fileName"_annotated_tmp2.txt
    # isConserved
    bedtools intersect -wa -wb -a "$fileName"_annotated_tmp2.txt -b "$4" -sorted -loj -g files/"$genome".chrom.sizes | awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4,$5,$6,$7,$11}' | uniq > "$fileName"_annotated_tmp3.txt

    nameOfNewFile_1="$(echo $(basename $fileName) | cut -d'_' -f1)"
    nameOfNewFile_2="$(echo $(basename $2) | cut -d'_' -f1)"
    nameOfNewFile="${nameOfNewFile_1}_annotatedW_${nameOfNewFile_2}_${genome}"

    sed "s/_peak_[[:digit:]]*//" "$fileName"_annotated_tmp3.txt | sed "s/_peak_[[:digit:]]*//" > intermediateFiles/${nameOfNewFile}.txt
    echo "New file created:" "intermediateFiles/${nameOfNewFile}.txt"

    rm "$fileName"_annotated_tmp2.txt "$fileName"_annotated_tmp3.txt intermediateFiles/"$(basename "$fileName" .bed)"_annotated_tmp0.txt

    # The new file created has this format: $5 acetylated in which tissue ; $6 TSS or exon ; $7 which CNEs
}

######
### Prepare conservation files with Galaxy
######

# Download .maf from UCSC
# mm vs gg : http://hgdownload.soe.ucsc.edu/goldenPath/mm10/vsGalGal6/mm10.galGal6.synNet.maf.gz
# This file contains the elements in mouse and in chicken, and it gives the same output as if starting from gg vs mm http://hgdownload.soe.ucsc.edu/goldenPath/galGal6/vsMm10/galGal6.mm10.synNet.maf.gz. So, there is no need of using both files.

# Galaxy Tool ID: MAF_To_Interval1
# Command Line: python '/data/galaxy/galaxy/server/tools/maf/maf_to_interval.py' '/data/galaxy/data/000/185/dataset_185974.dat' '/data/galaxy/data/000/185/dataset_185976.dat' '173164' '.' '?' 'galGal6,mm10' 'mm10,galGal6' partial_allowed keep_gaps
# Upload .maf in Galaxy. Use tool:  *MAF to Interval* (select mm10 and gg6 ; include blocks with missing species ; keep gaps). Then, tool *cut* on the output (c1,c2,c3,c6,c5 ; delimited by tab). Download "CSmm10_from_mm10.galGal6.synNet_unsorted.bed ".
# change datatype => bed , rename "CSmm10_from_mm10.galGal6.synNet_unsorted" and "CSgg6_from_mm10.galGal6.synNet_unsorted". Download

######
### Inputs
######

# all files must be sorted with a genome.chrom.sizes
workingDir=$1

# mouseGTF is avalaible in Zenodo (5917676)
mouseGTF=savedOutputs/files/Mus_musculus.GRCm38.99_simplified.gtf
mouseCS=files/CSmm10_from_mm10.galGal6.synNet_unsorted.bed

# chickenGTF is avalaible in Zenodo (5917720)
chickenGTF=savedOutputs/files/Gallus_gallus.GRCg6a.99_simplified.gtf
chickenCS=files/CSgg6_from_mm10.galGal6.synNet_unsorted.bed

# Get the narrowPeak files you want to take into consideration
mkdir -p intermediateFiles/narrowPeak_forCNEs_mm10
for f in ChIP_H3K27ac_E85_PT_rep1rep2.narrowPeak ChIP_H3K27ac_E125_WP_rep1.narrowPeak ChIP_H3K27ac_E125_DFL_rep1.narrowPeak ChIP_H3K27ac_E125_FB_rep1.narrowPeak ChIP_H3K27ac_E125_PFL_rep1.narrowPeak; do
    cp downloads/ChIP/$f intermediateFiles/narrowPeak_forCNEs_mm10/toSort_$f
    bedtools sort -i intermediateFiles/narrowPeak_forCNEs_mm10/toSort_$f -g files/mm10.chrom.sizes > intermediateFiles/narrowPeak_forCNEs_mm10/$f
    rm intermediateFiles/narrowPeak_forCNEs_mm10/toSort_$f
done

mkdir -p intermediateFiles/narrowPeak_forCNEs_gg6
for f in ChIP_H3K27ac_HH35_FB_rep1.narrowPeak ChIP_H3K27ac_HH28_PFL_rep1.narrowPeak ChIP_H3K27ac_HH28_DFL_rep1.narrowPeak ChIP_H3K27ac_HH18_PT_rep1rep2.narrowPeak ChIP_H3K27ac_HH35_DS_rep1rep2.narrowPeak; do
    cp downloads/ChIP/$f intermediateFiles/narrowPeak_forCNEs_gg6/toSort_$f
    bedtools sort -i intermediateFiles/narrowPeak_forCNEs_gg6/toSort_$f -g files/galGal6.chrom.sizes > intermediateFiles/narrowPeak_forCNEs_gg6/$f
    rm intermediateFiles/narrowPeak_forCNEs_gg6/toSort_$f
done

######
### Quantification of ARs
######

echo -e "\nGenerate and quantify Acetylated regions.\n"

### Mouse

# Give the same name to each CS in both genomes, i.e. the mouse sequence mm10_gg6_CNE_74 align to the chicken sequence mm10_gg6_CNE_74.
# Remove column with conservation score for compatibility.
bedtools sort -i $mouseCS -g files/mm10.chrom.sizes > intermediateFiles/CSmm10_from_mm10.galGal6.synNet.bed
sed 's/mm10/mm10_gg6/' intermediateFiles/CSmm10_from_mm10.galGal6.synNet.bed > intermediateFiles/tmp1.bed
sed 's/_0//' intermediateFiles/tmp1.bed > intermediateFiles/tmp2.bed
awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4}' intermediateFiles/tmp2.bed > intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed
rm intermediateFiles/tmp1.bed intermediateFiles/tmp2.bed

# Get TSS intervals (unique, non-overlapping)
# For this, the gtf used is the one from ncbiRefSeq
if [ ! -e files/mm10_all_TSS_2kbminus500bpplus_collapse.bed ]; then
    wget https://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/genes/mm10.ncbiRefSeq.gtf.gz -P downloads -nc
    gunzip -c downloads/mm10.ncbiRefSeq.gtf.gz | awk '
BEGIN{
    OFS= "\t"
}
{
    if($3=="transcript"){
        if ($7 == "+"){
            # GTF are 1-based
            print $1,$4-1,$4,NR,".",$7
        } else {
            print $1,$5-1,$5,NR,".",$7
        }
    }
}' | bedtools slop -l 2000 -r 499 -s -g files/mm10.chrom.sizes | bedtools sort | bedtools merge | awk 'BEGIN{OFS="\t"}{print $0,"TSS_"NR}' | bedtools sort -g files/mm10.chrom.sizes > files/mm10_all_TSS_2kbminus500bpplus_collapse.bed
fi

# Get exons intervals (unique names, non-overlapping, characterized as exons or TSS)
gtfToExonAndTSSbed $mouseGTF "files/mm10_all_TSS_2kbminus500bpplus_collapse.bed" "mm10"

# Generates genomic intervals from several files -  4 column bed
# Add a column 5 with 0's for compatibility 
generateGI "intermediateFiles/narrowPeak_forCNEs_mm10" "intermediateFiles/AR_mm10" "mm10"
# Annotates the genomic intervals with other features
annotateGI "intermediateFiles/AR_mm10.bed" "intermediateFiles/narrowPeak_forCNEs_mm10" "intermediateFiles/Mus_musculus.GRCm38.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed" "mm10"

### Chicken

# Give the same name to each CS in both genomes, i.e. the mouse sequence mm10_gg6_CNE_74 align to the chicken sequence mm10_gg6_CNE_74.
# Remove column with conservation score for compatibility.
bedtools sort -i $chickenCS -g files/galGal6.chrom.sizes > intermediateFiles/CSgg6_from_mm10.galGal6.synNet.bed 
sed 's/galGal6/mm10_gg6/' intermediateFiles/CSgg6_from_mm10.galGal6.synNet.bed > intermediateFiles/tmp1.bed
sed 's/_0//' intermediateFiles/tmp1.bed > intermediateFiles/tmp2.bed 
awk 'BEGIN{OFS = "\t"}{print $1,$2,$3,$4}' intermediateFiles/tmp2.bed > intermediateFiles/tmp3.bed
bedtools sort -i intermediateFiles/tmp3.bed -g files/galGal6.chrom.sizes > intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed 
rm intermediateFiles/tmp1.bed intermediateFiles/tmp2.bed intermediateFiles/tmp3.bed 

# Get TSS intervals (unique, non-overlapping)
# For this, the gtf used is the one from ncbiRefSeq
if [ ! -e files/galGal6_all_TSS_2kbminus500bpplus_collapse.bed ]; then
    # Unfortunately the file changed since 2020 while the bed file was generated in 2019
    # Therefore the bed cannot be reproduce with the version of the gtf online on 2022-05-13
    wget https://hgdownload.soe.ucsc.edu/goldenPath/galGal6/bigZips/genes/galGal6.ncbiRefSeq.gtf.gz -P downloads -nc
    gunzip -c downloads/galGal6.ncbiRefSeq.gtf.gz | awk '
BEGIN{
    OFS= "\t"
}
{
    if($3=="transcript"){
        if ($7 == "+"){
            # GTF are 1-based
            print $1,$4-1,$4,NR,".",$7
        } else {
            print $1,$5-1,$5,NR,".",$7
        }
    }
}' | bedtools slop -l 2000 -r 499 -s -g files/galGal6.chrom.sizes | bedtools sort | bedtools merge | awk 'BEGIN{OFS="\t"}{print $0,"TSS_"NR}' | bedtools sort -g files/galGal6.chrom.sizes > files/galGal6_all_TSS_2kbminus500bpplus_collapse.bed
fi

# Get exons intervals (unique names, non-overlapping, characterized as exons or TSS)
gtfToExonAndTSSbed $chickenGTF "files/galGal6_all_TSS_2kbminus500bpplus_collapse.bed" "galGal6"

# Generates genomic intervals from several files -  4 column bed
generateGI "intermediateFiles/narrowPeak_forCNEs_gg6" "intermediateFiles/AR_gg6" "galGal6"

# Annotates the genomic intervals with other features
annotateGI "intermediateFiles/AR_gg6.bed" "intermediateFiles/narrowPeak_forCNEs_gg6" "intermediateFiles/Gallus_gallus.GRCg6a.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed" "galGal6"

echo -e "\nPlot AR panel Fig5A.\n"
Rscript scripts/CNEs_011_quantif_ARs.R "$workingDir"/intermediateFiles/AR_annotatedW_narrowPeak_mm10.txt "$workingDir"/intermediateFiles/AR_annotatedW_narrowPeak_galGal6.txt

######
### Quantification of CNEs
######

echo -e "\nGenerate and quantify CNEs.\n"

# From CS to CNEs: keep only elements which are non-coding in both species.
bedtools intersect -v -a intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed -b $mouseGTF > intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named_mm10nonCoding.bed
bedtools intersect -v -a intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed -b $chickenGTF > intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named_gg6nonCoding.bed

# The ouptut of CNEs_012_CNS_nonCodingInBoth is a "CNE" file
Rscript scripts/CNEs_012_CNS_nonCodingInBoth.R "$workingDir"/intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named_mm10nonCoding.bed "$workingDir"/intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named_gg6nonCoding.bed

# Synteny plot
echo -e "\nSynteny plot (FigS3).\n"
Rscript scripts/CNEs_013_syntenyPlot.R "$workingDir"/intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named_CNEs.bed "$workingDir"/intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named_CNEs.bed

######
### Quantification of pCREs overlapping CNEs (pCREs are non-coding and H3K27ac+) (Fig5)
######

# Annotates the genomic intervals
annotateGI "intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed" "intermediateFiles/narrowPeak_forCNEs_mm10" "intermediateFiles/Mus_musculus.GRCm38.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed" "mm10"
annotateGI "intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed" "intermediateFiles/narrowPeak_forCNEs_gg6" "intermediateFiles/Gallus_gallus.GRCg6a.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed" "galGal6"

# Fig5B Rscript heatmap
# Prepare a bed to generate the matrix for pheatmap
# Usage is: Rscript scripts/CNEs_014_CNEs_prepBedForMx.R mm_CNEsAnnotations gg_CNEsAnnotations outputName
Rscript scripts/CNEs_014_CNEs_prepBedForMx.R "$workingDir"/intermediateFiles/CSmm10_annotatedW_narrowPeak_mm10.txt "$workingDir"/intermediateFiles/CSgg6_annotatedW_narrowPeak_galGal6.txt gw_mm_gg_CNEs_activeInAtLeastOne_forMx.txt

# Set the parameters in CNEs_015a_heatmap_Fig5B.conf.R
echo -e "\nParameters for the heatmap are in CNEs_015a_heatmap_Fig5B.conf.R.\n"
echo -e "\nPlot heatmap (fig5B).\n"
Rscript scripts/CNEs_015b_heatmap.R "$workingDir"/scripts/CNEs_015a_heatmap_Fig5B.conf.R

echo -e "\nEuler Plots (fig5C).\n"
# Fig5C Rscript euler
Rscript scripts/CNEs_016_euler.R "$workingDir"/intermediateFiles/CSmm10_annotatedW_narrowPeak_mm10.txt "$workingDir"/intermediateFiles/CSgg6_annotatedW_narrowPeak_galGal6.txt notOL


######
### Repeat the analyses of pCREs overlapping CNEs, but this time pCREs are H3K27ac+ / ATAC+. (FigS4 and FigS5)
######

# Get H3K27ac narrowPeaks which overlap ATAC peaks in matching tissues (note: ATAC-seq peaks are extended 660 bp both sides)
./scripts/CNEs_017_H3K27ac_OL_ATAC.sh

# Quantification of the proportion of H3K27ac ChIP-seq peaks that overlap ATAC-seq peaks in matching tissues (Fig.S5A)
Rscript scripts/CNEs_018_quantifOL.R "$workingDir"/savedOutputs/files/ATAC_quantif_ChIP_OL.txt

# Get narrowPeaks (H3K27ac+/ATAC+) that overlap with CNEs (FigS4)
# Mouse
for f in intermediateFiles/narrowPeakOL_ATAC_forCNEs_mm10_ext660/*; do
    bedtools intersect -a $f -b intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named_CNEs.bed -wa -wb > savedOutputs/files/$(basename $f .narrowPeak)_OL_CNEs.narrowPeak
done
#Chicken
for f in intermediateFiles/narrowPeakOL_ATAC_forCNEs_gg6_ext660/*; do
    bedtools intersect -a $f -b intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named_CNEs.bed -wa -wb > savedOutputs/files/$(basename $f .narrowPeak)_OL_CNEs.narrowPeak
done

####
## Heatmaps
####

# Annotate CNEs with H3K27ac peaks that overlap ATAC-seq peaks
annotateGI "intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed" "intermediateFiles/narrowPeakOL_ATAC_forCNEs_mm10" "intermediateFiles/Mus_musculus.GRCm38.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSmm10_from_mm10.galGal6.synNet_named.bed" "mm10"
annotateGI "intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed" "intermediateFiles/narrowPeakOL_ATAC_forCNEs_gg6" "intermediateFiles/Gallus_gallus.GRCg6a.99_simplified.exonsAndTSS_intervals.bed" "intermediateFiles/CSgg6_from_mm10.galGal6.synNet_named.bed" "galGal6"

# Prepare for matrix.
Rscript scripts/CNEs_014_CNEs_prepBedForMx.R "$workingDir"/intermediateFiles/CSmm10_annotatedW_narrowPeakOL_mm10.txt "$workingDir"/intermediateFiles/CSgg6_annotatedW_narrowPeakOL_galGal6.txt gw_mm_gg_CNEs_activeInAtLeastOne_forMx_OL_ATAC.txt

Rscript scripts/CNEs_015b_heatmap.R "$workingDir"/scripts/CNEs_015a_heatmap_FigS5B_OL.conf.R

# Use arg 3 "OL" to plot only tissues for which we have ATAC seq data. 
Rscript scripts/CNEs_016_euler.R "$workingDir"/intermediateFiles/CSmm10_annotatedW_narrowPeakOL_mm10.txt "$workingDir"/intermediateFiles/CSgg6_annotatedW_narrowPeakOL_galGal6.txt OL

rm tmp.txt